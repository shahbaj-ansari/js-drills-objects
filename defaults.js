function defaults(objects,defaultProps){
    if(objects==undefined){
        return []
    }
    for(let key in defaultProps){
        if(objects[key]==undefined){
            objects[key]=defaultProps[key]
        }
    }
    return objects
}

module.exports=defaults
