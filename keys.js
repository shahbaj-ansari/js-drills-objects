function keys(objects){
    if(objects==undefined){
        return []
    }
    let keysArray=[]
    for(let key in objects){
        keysArray.push(key)
    }
    return keysArray
}

module.exports=keys
