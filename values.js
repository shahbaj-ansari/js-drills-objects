function values(objects){
    if(objects==undefined){
        return []
    }
    let valuesArray=[]
    for(let key in objects){
        valuesArray.push(objects[key])
    }
    return valuesArray
}

module.exports=values
