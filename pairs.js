function pairs(objects){
    if(objects==undefined){
        return []
    }
    let pairsArray=[]
    for(let key in objects){
        pairsArray.push([key,objects[key]])
    }
    return pairsArray
}

module.exports=pairs
