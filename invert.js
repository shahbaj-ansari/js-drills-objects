
function invert(objects){
    if(objects==undefined){
        return []
    }
    let invertedObject={}
    for(let key in objects){
        let iKey=objects[key]
        invertedObject[iKey]=key
    }
    return invertedObject
}

module.exports=invert